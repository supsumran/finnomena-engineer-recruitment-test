
 ## 1 ใช้เวลาทำแบบทดสอบโดยประมาณ 7 ชั่วโมง ถ้ามีเวลามากขึ้นอยากจะให้สามารถ sort ค่าต่างๆได้ เช่น 
- เรียงรายการกองทุนที่ผลแทบแทนมากสุด สลับการน้อยสุด (ลองทำไว้แล้ว สามารถคลิกที่คำว่า ผลตอบแทน)
- เรียงรายการตามค่าอัปเดตของกอลทุน
- เรียงจากชื่อกอลทุน
- ออกแบบหน้า UI ให้ดีกว่านี้ (ผมเห็นว่าข้อมูล)
 ## 2 feature ในการพัฒนาแอปพลิเคชัน 
- feature การแสดงรายการกองทุนที่มีผลตอบแทนดีสุด โดยให้ใช้ retrofit2 + coroutine ในการดึงข้อมูลจาก API ใช้หลักการของ pattern Model View ViewModel(MVVM)  

## ส่วนติดตั้ง retrofit 

1. เพิ่ม dependencies
```Kotlin
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
```
2. สร้าง class RetrofitBuilder สำหรับ build retrofit เซ็คค่า BASE_URL
```Kotlin
object RetrofitBuilder {
private const val BASE_URL = "https://storage.googleapis.com/"
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}
```
3. สร้าง class ApiService เพื่อบอกว่า มีฟังก์ชันอะไรบ้างในการเรียก api โดยส่ง parameter ชื่อว่า time เช่น 1D, 1W, 1M, 1Y
```Kotlin
interface ApiService {
    @GET("finno-ex-re-v2-static-staging/recruitment-test/fund-ranking-{TIME}.json")
    suspend fun getDataFundsCollection(@Path("TIME") time: String): DataFundsCollection
}
```
4. สร้าง class ApiHelperImpl มีหน้าที่สำหรับ overrode ฟังก์ชันที่มีใน ApiHelper
```Kotlin
class ApiHelperImpl(private val apiService: ApiService) : ApiHelper {
    override suspend fun getDataFunds(time:String): DataFundsCollection {
        return apiService.getDataFundsCollection(time)
    }
}
```
5. สร้าง class ApiHelper เพื่อบอกว่า ApiHelper มีหน้าที่อะไรบ้าง
```Kotlin
interface ApiHelper {
    suspend fun getDataFunds(time:String) : DataFundsCollection
}
```

## ส่วนที่ดึงข้อมูลของกองทุนจาก API 

1. โดยส่ง ApiHelper เป็น parameter ผ่านทาง constructor มาที่ MainViewModel
และคำสั่ง viewModelScope.launch ในการเรียกใช้ฟังก์ชันดึงจาก ApiHelper

***MainViewModel.kt***
```Kotlin
fun fetchDataFunds() {
    viewModelScope.launch {
        dataFundsCollection.postValue(Resource.loading(null))
        try {
            val dataFromApi = apiHelper.getDataFunds(timeType.value!!)
            dataFundsCollection.postValue(Resource.success(dataFromApi))
        } catch (e: Exception) {
            dataFundsCollection.postValue(Resource.error(e.toString(), null))
        }
    }
}
```

2. จากนั่งเรียกใช้ฟังก์ชัน setupObserver() ใน MainActivity
สั่ง observe กับตัวแปรของ viewmodel เมื่อตัวแปรใน viewmodel มีการอัปเดต จะให้สามารถตรวจได้

***MainActivity.kt***
```Kotlin
private fun setupObserver() {
    viewModel.loadFunds().observe(this) {
        when (it.status) {
            Status.LOADING -> {
                viewModel.loading()
            }
            Status.SUCCESS -> {
                viewModel.success(it.data!!.data!!)
            }
            Status.ERROR -> {
                viewModel.error()
                Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    viewModel.dataFunds.observe(this) {
        fundsAdapter.setData(it)
        listFunds.scrollToPosition(0)
    }
}
```
สุดท้ายใช้คำสั่ง fundsAdapter.setData(it) สำหรับเซ็ตค่าให้กับ adapter

## 3 ติดตาม performance issue บน production 
ยังไม่เคยมีประสบการณ์ด้านนี้ครับ

## 4 อยากปรับปรุง FINNOMENA APIs ที่ใช้ในการพัฒนา ในส่วนไหนให้ดียิ่งขึ้น
ผมคิดว่าน่าจะมี parameter เป็นชื่อของกอลทุนนั้นๆ และสามารถดูประวัติผลตอบแทนในแต่ละช่วงเวลาได้

** ปล. ทำเป็น unit test ไว้ในไฟล์ ***MainViewModelTest.kt*** ใช้ Junit กับ mockito
เป็นเคสคร่าวๆ ของการดึงของมูลใน ***MainViewModel.kt*** ซึ่งฟังก์ชันไม่ได้เยอะมากครับ

วิธีติดตั้งโปรเจค 
1. clone จากลิงค์ https://gitlab.com/supsumran/finnomena-engineer-recruitment-test.git
2. เปิดโปรเจคโดยใช้ Android studio
3. build และ run

