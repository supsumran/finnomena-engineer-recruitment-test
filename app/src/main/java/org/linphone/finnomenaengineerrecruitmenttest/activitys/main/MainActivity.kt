package org.linphone.finnomenaengineerrecruitmenttest.activitys.main

import android.os.Bundle
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import org.linphone.finnomenaengineerrecruitmenttest.api.RetrofitBuilder
import kotlinx.android.synthetic.main.activity_main.*
import org.linphone.finnomenaengineerrecruitmenttest.R
import org.linphone.finnomenaengineerrecruitmenttest.activitys.main.adapter.FundsAdapter
import org.linphone.finnomenaengineerrecruitmenttest.api.ApiHelperImpl
import org.linphone.finnomenaengineerrecruitmenttest.databinding.ActivityMainBinding
import org.linphone.finnomenaengineerrecruitmenttest.utils.Status

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    lateinit var fundsAdapter: FundsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this

        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                getString(R.string.one_year_type)
            )
        )[MainViewModel::class.java]

        binding.viewModel = viewModel

        setupView()
        setupObserver()
    }

    private fun setupView() {
        fundsAdapter = FundsAdapter()

        listFunds.apply {
            layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            adapter = fundsAdapter
        }

        // default time to 1 year
        ((timeGroup.getChildAt(0)) as RadioButton).isChecked = true
        viewModel.timeType.observe(this) {
            listFunds.scrollToPosition(0)
        }
    }

    private fun setupObserver() {
        viewModel.loadFunds().observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    viewModel.loading()
                }
                Status.SUCCESS -> {
                    viewModel.success(it.data!!.data!!)
                }
                Status.ERROR -> {
                    viewModel.error()
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }

        viewModel.dataFunds.observe(this) {
            fundsAdapter.setData(it)
            listFunds.scrollToPosition(0)
        }
    }
}