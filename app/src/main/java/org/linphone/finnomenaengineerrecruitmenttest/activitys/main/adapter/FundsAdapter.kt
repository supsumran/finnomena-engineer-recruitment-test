package org.linphone.finnomenaengineerrecruitmenttest.activitys.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fund_item.view.*
import org.linphone.finnomenaengineerrecruitmenttest.MyApp
import org.linphone.finnomenaengineerrecruitmenttest.R
import org.linphone.finnomenaengineerrecruitmenttest.models.Funds
import org.linphone.finnomenaengineerrecruitmenttest.utils.TimeUtil

class FundsAdapter : RecyclerView.Adapter<FundsAdapter.ViewHolder>() {

    private var data: List<Funds?>? = null

    init {
        data = arrayListOf()
    }

    fun setData(data: List<Funds?>?) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.fund_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data!![position]!!, position)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(funds: Funds, position: Int) {
            itemView.thailandFundCode.text = "${position+1}. ${funds.thailandFundCode}"
            itemView.nav.text = "${MyApp.getContext()!!.getString(R.string.price_title)} : ${funds.nav.toString()}"
            itemView.navDate.text = TimeUtil.dateStringConvertToDateFormat(funds.navDate.toString())
            itemView.avgReturn.text = funds.navReturn.toString()
        }
    }
}