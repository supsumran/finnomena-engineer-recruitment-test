package org.linphone.finnomenaengineerrecruitmenttest.activitys.main

import android.view.View
import android.widget.RadioGroup
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.linphone.finnomenaengineerrecruitmenttest.api.ApiHelper
import org.linphone.finnomenaengineerrecruitmenttest.models.DataFundsCollection
import org.linphone.finnomenaengineerrecruitmenttest.models.Funds
import org.linphone.finnomenaengineerrecruitmenttest.utils.Resource
import androidx.lifecycle.ViewModel as ViewModel1

class ViewModelFactory(private val apiHelper: ApiHelper, private val time: String) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(apiHelper, time) as T
    }
}

class MainViewModel(private val apiHelper: ApiHelper, time: String) : ViewModel1() {

    val dataFunds = MutableLiveData<List<Funds?>?>()
    val timeType = MutableLiveData<String>()
    val isASC = MutableLiveData<Boolean>()
    val isTryAgain = MutableLiveData<Boolean>()
    val isProgress = MutableLiveData<Boolean>()

    private val dataFundsCollection = MutableLiveData<Resource<DataFundsCollection>>()

    init {
        isASC.value = false
        timeType.value = time
        isTryAgain.value = false
        fetchDataFunds()
    }

    fun fetchDataFunds() {
        viewModelScope.launch {
            dataFundsCollection.postValue(Resource.loading(null))
            try {
                val dataFromApi = apiHelper.getDataFunds(timeType.value!!)
                dataFundsCollection.postValue(Resource.success(dataFromApi))
            } catch (e: Exception) {
                dataFundsCollection.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun setFunds(data: List<Funds?>?) {
        dataFunds.value = data
    }

    fun loadFunds(): LiveData<Resource<DataFundsCollection>> {
        return dataFundsCollection
    }

    fun selectTimeOption(radioGroup: RadioGroup, radioButton: View) {
        radioGroup.check(radioButton.id)
        timeType.value = radioButton.tag.toString()
        fetchDataFunds()
    }

    fun toggleSortByNavReturn() {
        isASC.value = swapSortMode(isASC.value!!)
        if (isASC.value!!) {
            dataFunds.value = dataFunds.value!!.sortedBy { it!!.navReturn }
        } else {
            dataFunds.value = dataFunds.value!!.sortedByDescending { it!!.navReturn }
        }
    }

    fun swapSortMode(isASC: Boolean): Boolean {
        return !isASC
    }

    fun loading() {
        isTryAgain.value = false
        isProgress.value= true
    }

    fun error() {
        isTryAgain.value = true
        isProgress.value= false
    }

    fun success(data: List<Funds?>?) {
        isTryAgain.value = false
        isProgress.value = false
        setFunds(data!!)
    }
}