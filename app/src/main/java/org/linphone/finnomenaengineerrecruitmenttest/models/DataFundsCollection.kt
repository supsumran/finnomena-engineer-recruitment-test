package org.linphone.finnomenaengineerrecruitmenttest.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DataFundsCollection {
    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("error")
    @Expose
    var error: String? = null

    @SerializedName("data")
    @Expose
    var data: List<Funds?>? = null

}