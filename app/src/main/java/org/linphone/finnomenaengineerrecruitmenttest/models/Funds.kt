package org.linphone.finnomenaengineerrecruitmenttest.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Funds {
    @SerializedName("mstar_id")
    @Expose
    var mstarId: String? = null

    @SerializedName("thailand_fund_code")
    @Expose
    var thailandFundCode: String? = null

    @SerializedName("nav_return")
    @Expose
    var navReturn: Double? = null

    @SerializedName("nav")
    @Expose
    var nav: Double? = null

    @SerializedName("nav_date")
    @Expose
    var navDate: String? = null

    @SerializedName("avg_return")
    @Expose
    var avgReturn: Double? = null

}