package org.linphone.finnomenaengineerrecruitmenttest.utils

import android.util.Log
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class TimeUtil {
    companion object {

        //iso format
        fun dateStringConvertToDateFormat(str_date : String) : String {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val outputFormat = SimpleDateFormat("dd-MM-yyyy")
            val date = inputFormat.parse(str_date)
            val formattedDate = outputFormat.format(date)
//            Log.d("dateStringConvertToDateFormat", "date: ${date.time} : $formattedDate")
            return  formattedDate
        }
    }
}