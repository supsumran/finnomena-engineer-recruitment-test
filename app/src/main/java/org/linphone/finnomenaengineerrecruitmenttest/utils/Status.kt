package org.linphone.finnomenaengineerrecruitmenttest.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}