package org.linphone.finnomenaengineerrecruitmenttest

import android.app.Application
import android.content.Context

class MyApp : Application() {

    companion object {
        private var context: Context? = null
        fun getContext(): Context? {
            return this.context
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}