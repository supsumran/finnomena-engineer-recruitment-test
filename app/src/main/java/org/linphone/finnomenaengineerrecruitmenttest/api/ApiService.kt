package org.linphone.finnomenaengineerrecruitmenttest.api

import org.linphone.finnomenaengineerrecruitmenttest.models.DataFundsCollection
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("finno-ex-re-v2-static-staging/recruitment-test/fund-ranking-{TIME}.json")
    suspend fun getDataFundsCollection(@Path("TIME") time: String): DataFundsCollection

}