package org.linphone.finnomenaengineerrecruitmenttest.api

import org.linphone.finnomenaengineerrecruitmenttest.models.DataFundsCollection

class ApiHelperImpl(private val apiService: ApiService) : ApiHelper {

    override suspend fun getDataFunds(time:String): DataFundsCollection {
        return apiService.getDataFundsCollection(time)
    }

}