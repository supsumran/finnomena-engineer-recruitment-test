package org.linphone.finnomenaengineerrecruitmenttest.api

import org.linphone.finnomenaengineerrecruitmenttest.models.DataFundsCollection


interface ApiHelper {

    suspend fun getDataFunds(time:String) : DataFundsCollection

}