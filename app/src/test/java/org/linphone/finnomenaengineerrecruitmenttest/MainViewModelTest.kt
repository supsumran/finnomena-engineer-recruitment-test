package org.linphone.finnomenaengineerrecruitmenttest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.linphone.finnomenaengineerrecruitmenttest.activitys.main.MainViewModel
import org.linphone.finnomenaengineerrecruitmenttest.api.ApiHelper
import org.linphone.finnomenaengineerrecruitmenttest.models.DataFundsCollection
import org.linphone.finnomenaengineerrecruitmenttest.utils.Resource
import org.linphone.finnomenaengineerrecruitmenttest.utils.TestCoroutineRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var apiHelper : ApiHelper

    @Mock
    private lateinit var apiUsersObserver: Observer<Resource<DataFundsCollection>>

    @Before
    fun setUp() {
        // do something if required
    }

    @Test
    fun test_server_response_200_when_fetch_1D_should_return_success() {
        testCoroutineRule.runBlockingTest {
            var time = "1D"
            var dataCollectionSuccess = DataFundsCollection()
            Mockito.doReturn(dataCollectionSuccess)
                .`when`(apiHelper)
                .getDataFunds(time)

            val viewModel = MainViewModel(apiHelper, time)
            viewModel.loadFunds().observeForever(apiUsersObserver)
            Mockito.verify(apiHelper).getDataFunds(time)
            Mockito.verify(apiUsersObserver).onChanged(Resource.success(dataCollectionSuccess))
            viewModel.loadFunds().removeObserver(apiUsersObserver)
        }
    }

    @Test
    fun test_server_response_200_when_fetch_1W_should_return_success() {
        testCoroutineRule.runBlockingTest {
            var time = "1W"
            var dataCollectionSuccess = DataFundsCollection()

            Mockito.doReturn(dataCollectionSuccess)
                .`when`(apiHelper)
                .getDataFunds(time)
            val viewModel = MainViewModel(apiHelper, time)
            viewModel.loadFunds().observeForever(apiUsersObserver)
            Mockito.verify(apiHelper).getDataFunds(time)
            Mockito.verify(apiUsersObserver).onChanged(Resource.success(dataCollectionSuccess))
            viewModel.loadFunds().removeObserver(apiUsersObserver)
        }
    }

    @Test
    fun test_server_response_200_when_fetch_1M_should_return_success() {
        testCoroutineRule.runBlockingTest {
            var time = "1M"
            var dataCollectionSuccess = DataFundsCollection()

            Mockito.doReturn(dataCollectionSuccess)
                .`when`(apiHelper)
                .getDataFunds(time)
            val viewModel = MainViewModel(apiHelper, time)
            viewModel.loadFunds().observeForever(apiUsersObserver)
            Mockito.verify(apiHelper).getDataFunds(time)
            Mockito.verify(apiUsersObserver).onChanged(Resource.success(dataCollectionSuccess))
            viewModel.loadFunds().removeObserver(apiUsersObserver)
        }
    }

    @Test
    fun test_server_response_200_when_fetch_1Y_should_return_success() {
        testCoroutineRule.runBlockingTest {
            var time = "1Y"
            var dataCollectionSuccess = DataFundsCollection()

            Mockito.doReturn(dataCollectionSuccess)
                .`when`(apiHelper)
                .getDataFunds(time)
            val viewModel = MainViewModel(apiHelper, time)
            viewModel.loadFunds().observeForever(apiUsersObserver)
            Mockito.verify(apiHelper).getDataFunds(time)
            Mockito.verify(apiUsersObserver).onChanged(Resource.success(dataCollectionSuccess))
            viewModel.loadFunds().removeObserver(apiUsersObserver)
        }
    }

    @Test
    fun test_server_response_error_when_fetch_should_return_error() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Error load data"
            var time = "1D"

            Mockito.doThrow(RuntimeException(errorMessage))
                .`when`(apiHelper)
                .getDataFunds(time)
            val viewModel = MainViewModel(apiHelper, time)
            viewModel.loadFunds().observeForever(apiUsersObserver)
            Mockito.verify(apiHelper).getDataFunds(time)
            Mockito.verify(apiUsersObserver).onChanged(
                Resource.error(
                    RuntimeException(errorMessage).toString(),
                    null
                )
            )
            viewModel.loadFunds().removeObserver(apiUsersObserver)
        }
    }

    @Test
    fun test_toggle_isASC_should_return_true() {
        var time = "1D"
        var isASC = false
        val viewModel = MainViewModel(apiHelper, time)
        Assert.assertEquals(true, viewModel.swapSortMode(isASC))
    }

    @Test
    fun test_toggle_isASC_should_return_false() {
        var time = "1D"
        var isASC = true
        val viewModel = MainViewModel(apiHelper, time)
        Assert.assertEquals(false, viewModel.swapSortMode(isASC))
    }

    @After
    fun tearDown() {
        // do something if required
    }
}